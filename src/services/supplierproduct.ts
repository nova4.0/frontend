import type Supplierproduct from "../types/Supplierproduct";
import http from "./axios";
function getSupplierproductsBySupplier(supplier: number) {
  return http.get(`/supplierproducts/supplier/${supplier}`);
}
function getSupplierproductsById(id: number) {
  return http.get(`/supplierproducts/${id}`);
}
function getSupplierproducts() {
  return http.get("/supplierproducts");
}
export default {
  getSupplierproducts,
  getSupplierproductsBySupplier,
  getSupplierproductsById,
};