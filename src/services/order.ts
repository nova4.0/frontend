import type Order from "@/types/Order";
import http from "./axios";

// function saveOrder(cartId: number, address: string, paymentMethodId: number, deliveryMethodId: number, discount: number) {
//   return http.post(`orders/${cartId}/${address}/${paymentMethodId}/${deliveryMethodId}/${discount}`);
// }
function saveOrder(cartId: number, address: string, paymentMethodId: number, deliveryMethodId: number, discount: number, slipImage: string) {
  return http.post(`orders/${cartId}/${address}/${paymentMethodId}/${deliveryMethodId}/${discount}/${slipImage}`);
}
function saveOrder1(order: Order & { files: File[] }) {
  const formData = new FormData();
  formData.append("discount", `${order.discount}`);
  formData.append("subtotal", `${order.subtotal}`);
  formData.append("totalPrice", `${order.totalPrice}`);
  formData.append("address", order.address);
  formData.append("file", order.files[0]);
  formData.append("cartId", `${order.cartId}`);
  formData.append("paymentMethodId", `${order.paymentMethodId}`);
  formData.append("deliveryMethodId", `${order.deliveryMethodId}`);

  return http.post("/orders", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

function getOrderById(id: number) {
  return http.get(`/orders/${id}`);
}
function getOrders() {
  return http.get("/orders");
}
export default { saveOrder, saveOrder1, getOrderById, getOrders};
