import type Cart from "@/types/Cart";
import http from "./axios";
function getCartItem(cartId: number) {
  return http.get(`/cart/${cartId}/items`);
}
function getCart(cartId: number) {
  return http.get(`/cart/${cartId}/items`);
}
function saveProductToCart(cartId: number, productId: number, quantity: number) {
  return http.post(`cart/${cartId}/add-item/${productId}/${quantity}`);
}
function saveCart(customerId: number) {
  return http.post(`/cart/${customerId}`, customerId);
}
function updateCartItemQuantity(quantity: number, itemId: number) {
  return http.patch(`/cart/${quantity}/items/${itemId}`);
}
function deleteCartItem(itemId: number) {
  return http.delete(`/cart/items/${itemId}`);
}
export default { getCartItem, saveCart, updateCartItemQuantity, deleteCartItem, saveProductToCart, getCart };
