import type Supplier from "@/types/Category";
import http from "./axios";
function getSupplier() {
  return http.get("/supplier");
}
function getSupplierById(id: number) {
  return http.get(`/supplier/${id}`);
}
export default { getSupplier,getSupplierById};