import type Stockproduct from "../types/stockproduct"
import http from "./axios";
function getStockproduct() {
  return http.get("/Stockproduct");
}

function saveStockproduct(Stockproduct: Stockproduct) {
  return http.post("/Stockproduct", Stockproduct);
}

function updateStockproduct(id: number, Stockproduct: Stockproduct) {
  return http.patch(`/Stockproduct/${id}`, Stockproduct);
}

function deleteStockproduct(id: number) {
  return http.delete(`/Stockproduct/${id}`);
}
function getStockproductByProduct(product: number) {
  return http.get(`/Stockproduct/product/${product}`);
}

export default { getStockproduct, saveStockproduct, updateStockproduct, deleteStockproduct ,getStockproductByProduct};
