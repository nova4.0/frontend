import type Promotion from "../types/Promotion";
import http from "./axios";

function getPromotions() {
  return http.get("/promotion");
}
// function savePromotion(promotion: Promotion) {
//   const formData = new FormData();
//   formData.append("name", promotion.name);
//   formData.append("description", promotion.description);
//   formData.append("discount", `${promotion.discount}`);
//   formData.append("startDate", `${promotion.startDate}`);
//   formData.append("endDate", `${promotion.endDate}`);
//   return http.post("/promotion", formData, {
//     headers: {
//       "Content-Type": "multipart/form-data",
//     },
//   });
// }
function savePromotion(Promotion: Promotion) {
  return http.post("/Promotion", Promotion);
}
// function updatePromotion(id: number, promotion: Promotion) {
//   const formData = new FormData();
//   formData.append("name", promotion.name);
//   formData.append("description", promotion.description);
//   formData.append("discount", `${promotion.discount}`);
//   formData.append("startDate", `${promotion.startDate}`);
//   formData.append("endDate", `${promotion.endDate}`);
//   return http.patch(`/promotion/${id}`, formData, {
//     headers: {
//       "Content-Type": "multipart/form-data",
//     },
//   });
// }
function updatePromotion(id: number, Promotion: Promotion) {
  return http.patch(`/Promotion/${id}`, Promotion);
}

function deletePromotion(id: number) {
  return http.delete(`/promotion/${id}`);
}

export default {
  getPromotions,
  savePromotion,
  updatePromotion,
  deletePromotion,
};
