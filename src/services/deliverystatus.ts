import http from "./axios";

function getDeliverystatus() {
    return http.get("/deliverystatus");
}


export default {
    getDeliverystatus
};
