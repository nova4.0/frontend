import http from "./axios";

function getDeliverymethod() {
    return http.get("/deliverymethod");
}


export default {
    getDeliverymethod
};
