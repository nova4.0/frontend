import http from "./axios";

function getPaymentmethod() {
    return http.get("/paymentmethod");
}


export default {
    getPaymentmethod
};
