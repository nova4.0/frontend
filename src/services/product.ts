import type Product from "../types/Product";
import http from "./axios";
function getProductsByCategory(category: number) {
  return http.get(`/products/category/${category}`);
}
function getProductsById(id: number) {
  return http.get(`/products/${id}`);
}
function getProducts() {
  return http.get("/products");
}
function saveProduct(product: Product & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", product.name);
  formData.append("description", product.description);
  formData.append("price", `${product.price}`);
  formData.append("minquantity", `${product.minquantity}`);
  formData.append("qtybefore", `${product.qtybefore}`);
  formData.append("file", product.files[0]);
  formData.append("categoryId", `${product.categoryId}`);
  return http.post("/products", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}
function updateProduct(id: number, product: Product & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", product.name);
  formData.append("description", product.description);
  formData.append("price", `${product.price}`);
  formData.append("minquantity", `${product.minquantity}`);
  formData.append("qtybefore", `${product.qtybefore}`);
  formData.append("file", product.files[0]);
  formData.append("categoryId", `${product.categoryId}`);
  return http.patch(`/products/${id}`, formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

function deleteProduct(id: number) {
  return http.delete(`/products/${id}`);
}

function updateProductQuantity(id: number, quantity: number){
  return http.patch(`/products/${quantity}/${id}`);
}

export default {
  getProducts,
  saveProduct,
  updateProduct,
  deleteProduct,
  getProductsByCategory,
  getProductsById,
  updateProductQuantity
};
