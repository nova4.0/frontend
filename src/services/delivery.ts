import type Delivery from '@/types/Delivery'
import http from './axios'

function saveDelivery(delivery: Delivery) {
  return http.post('/delivery', delivery)
}
function getDeliverys() {
  return http.get('/delivery')
}
function updateDeliverys(id: number, delivery: Delivery) {
  return http.patch(`/delivery/${id}`, delivery)
}

function getDeliveryById(id: number) {
  return http.get(`/delivery/${id}`)
}
export default {
  saveDelivery,
  getDeliverys,
  getDeliveryById,
  updateDeliverys
}
