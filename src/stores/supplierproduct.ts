import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Supplierproduct from "../types/Supplierproduct";
import supplierproductService from "../services/supplierproduct";
// import { useLoadingStore } from "./loading";
// import { useMessageStore } from "./message";
import Swal from "sweetalert2";

export const useSupplierproductStore = defineStore("Supplierproduct", () => {
  // const loadingStore = useLoadingStore();
  // const messageStore = useMessageStore();
  const page = ref(1);
  const take = ref(5);
  const keyword = ref("");
  const productGetID = ref(1);
  const lastPage = ref(3);
  const dialog = ref(false);
  const supplierproductId = ref(0);
  const count = ref(1);
  const supplierproducts = ref<Supplierproduct[]>([]);
  const supplier = ref(1);
  const orderList = ref<
    { supplierproduct: Supplierproduct; amount: number; sum: number; price: number }[]
  >([]);
  function addOrderItem(item: Supplierproduct) {
    for (let i = 0; i < orderList.value.length; i++) {
      if (orderList.value[i].supplierproduct.id === item.id) {
        orderList.value[i].amount++;
        orderList.value[i].sum = orderList.value[i].amount * item.price;
        console.log(orderList.value[i].amount)
        return;
      }
    }

    orderList.value.push({
      supplierproduct: item,
      amount: 1,
      sum: item.price,
      price: item.price,
    });
    console.log(orderList)
  }
  function deleteOrderItem(index: number) {
    orderList.value.splice(index, 1);
  }
  function clearOrderItem() {
    orderList.value = [];
  }
  function increaseCount() {
    count.value++;
  }
  function decreaseCount() {
    if (count.value > 1) {
      count.value--;
    }
  }
  watch(supplier, async (newSupplier, oldSupplier) => {
    await getSupplierproductsBySupplier(newSupplier);
  });

  async function getSupplierproductsBySupplier(supplier: number) {
    // loadingStore.isLoading = true;
    try {
      const res = await supplierproductService.getSupplierproductsBySupplier(supplier);
      supplierproducts.value = res.data;
      console.log(supplierproducts.value);
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    // loadingStore.isLoading = false;
  }

  async function getSupplierproducts() {
    // loadingStore.isLoading = true;
    try {
      const res = await supplierproductService.getSupplierproducts();
      supplierproducts.value = res.data;

      console.log(supplierproducts.value);
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    // loadingStore.isLoading = false;
  }
  const sumPrice = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + (orderList.value[i].supplierproduct.price * orderList.value[i].amount);
    }
    return sum;
  });

  return {
    supplierproducts,
    getSupplierproducts,
    dialog,
    supplier,
    getSupplierproductsBySupplier,
    page,
    take,
    keyword,
    lastPage,
    productGetID,
    supplierproductId,
    count,
    increaseCount,
    decreaseCount,
    orderList,
    addOrderItem,
    deleteOrderItem,
    sumPrice,
    clearOrderItem,
    // product
  };
});
