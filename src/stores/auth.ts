import { ref, computed } from "vue";
import { defineStore } from "pinia";
import auth from "@/services/auth";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import authService from "@/services/auth";
import router from "@/router";
import type User from "@/types/User";

export const useAuthStore = defineStore("auth", () => {
  const dialog = ref(false);
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const authName = ref("");
  const username = ref("");
  const password = ref("");

  const getUser = () => {
    const userString = localStorage.getItem("user");
    if (!userString) return null;
    const user = JSON.parse(userString ?? "");
    return user;
  };

  const isLogin = () => {
    const user = localStorage.getItem("user");
    if (user) {
      return true;
    }
    return false;
  };
  const login = async (): Promise<void> => {
    loadingStore.isLoading = true;
    try {
      const res = await authService.login(username.value, password.value);
      const userData = res.data.user
      if (userData) {
        localStorage.setItem('userData', JSON.stringify(userData))
        localStorage.setItem('access_token', JSON.stringify(userData.access_token))
        localStorage.setItem('id', userData.id)
        localStorage.setItem('username', userData.username)
        messageStore.showConfirm('เข้าสู่ระบบเรียบร้อย')
        router.push('/')
      }
    } catch (e) {
      messageStore.showError("Username หรือ Password ไม่ถูกต้อง");
    }
    loadingStore.isLoading = false;
    localStorage.setItem("token", username.value);
  };
  const logout = () => {
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    router.replace("/login");
  };

  return { login, logout, isLogin, getUser,dialog };
});
