import { ref, watch } from "vue";
import { defineStore } from "pinia";

import type Stockproduct from "../types/stockproduct"
import StockproductService from "../services/stockproduct";
import stockproduct from "../services/stockproduct";
// import { useLoadingStore } from "./loading";
// import { useMessageStore } from "./message";

export const useStockproductStore = defineStore("Stockproduct", () => {
  // const loadingStore = useLoadingStore();
  // const messageStore = useMessageStore();
  const dialog = ref(false);
  const Stockproducts = ref<Stockproduct[]>([]);
  const product = ref(1);
  const editedStockproduct = ref<Stockproduct>({
    qtyafter: 0,
    productId: 0,
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedStockproduct.value = {
        qtyafter: 0,
        productId: 1,
      };
    }
  });
  async function getStockproductByProduct(product: number) {
    // loadingStore.isLoading = true;
    try {
      const res = await StockproductService.getStockproductByProduct(product);
      Stockproducts.value = res.data;
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    // loadingStore.isLoading = false;
  }
  async function getStockproduct() {
    // loadingStore.isLoading = true;
    try {
      const res = await StockproductService.getStockproduct();
      Stockproducts.value = res.data;
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถดึงข้อมูล Stockproduct ได้");
    }
    // loadingStore.isLoading = false;
  }
  async function saveStockproduct() {
    // loadingStore.isLoading = true;
    try {
      if (editedStockproduct.value.id) {
        const res = await StockproductService.updateStockproduct(
          editedStockproduct.value.id,
          editedStockproduct.value
        );
      } else {
        const res = await StockproductService.saveStockproduct(editedStockproduct.value);
      }

      dialog.value = false;

      await getStockproduct();
    } catch (e) {
      // messageStore.showError("ไม่สามารถบันทึก Stockproduct ได้");
      console.log(e);
    }
    window.location.reload();
    // loadingStore.isLoading = false;
  }
  async function deleteStockproduct(id: number) {
    // loadingStore.isLoading = true;
    try {
      const res = await StockproductService.deleteStockproduct(id);
      await getStockproduct();
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถลบ Stockproduct ได้");
    }
    window.location.reload();
    // loadingStore.isLoading = false;
  }

  function editStockproduct(Stockproduct: Stockproduct) {
    editedStockproduct.value = JSON.parse(JSON.stringify(Stockproduct));
    dialog.value = true;
  }
  return {
    Stockproducts,
    getStockproduct,
    dialog,
    editedStockproduct,
    saveStockproduct,
    editStockproduct,
    deleteStockproduct,
    getStockproductByProduct,
  };
});
