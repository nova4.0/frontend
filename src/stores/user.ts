import { ref, watch } from "vue";
import { defineStore } from "pinia";
import userService from "../services/user";
import type User from "@/types/User";
import Swal from "sweetalert2";
export const useUserStore = defineStore("User", () => {
    // const loadingStore = useLoadingStore();
    // const messageStore = useMessageStore();
    const dialog = ref(false);
    const user = ref<User[]>([]);
    const userId = 1
    const editedUser = ref<User>({ name: "",  password: "",
    telNumber: "",
    address: "",
    cd_creditcardnumber: "",
    cd_expirationdate: "",
    cd_name: "",
    cd_CVVnumber: "",
    cd_address: "", });
  
    watch(dialog, (newDialog, oldDialog) => {
      console.log(newDialog);
      if (!newDialog) {
        editedUser.value = {  name: "",  password: "",
        telNumber: "",
        address: "",
        cd_creditcardnumber: "",
        cd_expirationdate: "",
        cd_name: "",
        cd_CVVnumber: "",
        cd_address: "", };
      }
    });
    async function getUsers() {
      // loadingStore.isLoading = true;
      try {
        const res = await userService.getUsers();
        user.value = res.data;
      } catch (e) {
        console.log(e);
        // messageStore.showError("ไม่สามารถดึงข้อมูล UsergetUsers ได้");
      }
      // loadingStore.isLoading = false;
    }
  
    async function saveUser() {
      // loadingStore.isLoading = true;
      try {
        if (editedUser.value.id) {
          const res = await userService.updateUser(
            editedUser.value.id,
            editedUser.value
          );
        } else {
          const res = await userService.saveUser(editedUser.value);
        }
  
        dialog.value = false;
        await getUsers();
      } catch (e) {
        // messageStore.showError("ไม่สามารถบันทึก UsergetUsers ได้");
        console.log(e);
      }
      // loadingStore.isLoading = false;
    }
  
    async function deleteUsergetUsers(id: number) {
      const confirmed = await Swal.fire({
        title: "Are you sure?",
        text: "You will not be able to recover this user!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      });
  
      if (confirmed.isConfirmed) {
        // loadingStore.isLoading = true;
        try {
          const res = await userService.deleteUser(id);
          await getUsers();
          // messageStore.showConfirm("UsergetUsers deleted successfully");
        } catch (e) {
          console.log(e);
          // messageStore.showError("Failed to delete user");
        }
        // loadingStore.isLoading = false;
      }
    }
    function editUsergetUsers(user: User) {
      editedUser.value = JSON.parse(JSON.stringify(user));
      dialog.value = true;
    }
    async function getUserById() {
      // loadingStore.isLoading = true;
      try {
          const res = await userService.getUserById(userId);
          user.value = res.data;

          console.log("User",user.value);
      } catch (e) {
          console.log(e);
      }
  }
    return {
      getUserById,
      user,
      userId,
      getUsers,
      dialog,
      editedUser,
      saveUser,
      editUsergetUsers,
      deleteUsergetUsers,
    };
  });
