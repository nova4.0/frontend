import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "../types/Product";
import productService from "../services/product";
// import { useLoadingStore } from "./loading";
// import { useMessageStore } from "./message";
import Swal from "sweetalert2";

export const useProductStore = defineStore("Product", () => {
  // const loadingStore = useLoadingStore();
  // const messageStore = useMessageStore();
  const page = ref(1);
  const take = ref(5);
  const keyword = ref("");
  const productGetID = ref(1);
  const lastPage = ref(3);
  const dialog = ref(false);
  const productId = ref(0);
  const count = ref(1);
  // const product = ref<Product[]>([]);
  const products = ref<Product[]>([]);
  const category = ref(1);
  function increaseCount() {
    count.value++;
  }
  function decreaseCount() {
    if (count.value > 1) {
      count.value--;
    }
  }

  const gotProduct = ref<Product & { files: File[] }>({
    name: "",
    description: "",
    price: 0.0,
    minquantity: 0,
    qtybefore: 0,
    image: "no_img_avaliable.jpg",
    files: [],
    categoryId: 1,
  });

  const editedProduct = ref<Product & { files: File[] }>({
    name: "",
    description: "",
    price: 0.0,
    minquantity: 0,
    qtybefore: 0,
    image: "no_img_avaliable.jpg",
    files: [],
    categoryId: 1,
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedProduct.value = {
        name: "",
        description: "",
        price: 0.0,
        minquantity: 0,
        qtybefore: 0,
        image: "no_img_avaliable.jpg",
        files: [],
        categoryId: 1,
      };
    }
  });

  watch(category, async (newCategory, oldCategory) => {
    await getProductsByCategory(newCategory);
  });

  async function getProductsByCategory(category: number) {
    // loadingStore.isLoading = true;
    try {
      const res = await productService.getProductsByCategory(category);
      products.value = res.data;
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    // loadingStore.isLoading = false;
  }


  async function getProducts() {
    // loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts();
      products.value = res.data;

      console.log(products.value);
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    // loadingStore.isLoading = false;
  }

  async function saveProduct() {
    // loadingStore.isLoading = true;
    try {
      if (editedProduct.value.id) {
        const res = await productService.updateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        const res = await productService.saveProduct(editedProduct.value);
      }

      dialog.value = false;
      await getProducts();
    } catch (e) {
      // messageStore.showError("ไม่สามารถบันทึก Product ได้");
      console.log(e);
    }
    // loadingStore.isLoading = false;
  }

  async function deleteProduct(id: number) {
    try {
      const result = await Swal.fire({
        title: "Are you sure?",
        text: "You will not be able to recover this product!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d33",
        cancelButtonColor: "#3085d6",
        confirmButtonText: "Yes",
        cancelButtonText: "Cancel",
      });
      if (result.isConfirmed) {
        // loadingStore.isLoading = true;
        const res = await productService.deleteProduct(id);
        await getProducts();
        // messageStore.showConfirm("ลบ Product เรียบร้อยแล้ว");
      }
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถลบ Product ได้");
    } finally {
      // loadingStore.isLoading = false;
    }
  }
  function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }
  // function getProductDetail(product: Product) {
  //   gotProduct.value = JSON.parse(JSON.stringify(product));
  //   productId.value = product.id
  //   console.log(product)
  // }
  function getProductDetail(product: Product) {
    gotProduct.value = JSON.parse(JSON.stringify(product));
    if (product.id !== undefined) {
      productId.value = product.id;
    }
    console.log(product);
  }

  const addProductQuantityCount = ref("");
  const productQuantity = ref(0);
  const addProductQuantityStatus = ref(false);
  async function addProductQuantity(id: number, addQuantityCount: string) {
    console.log(id)
    console.log(addQuantityCount)
    console.log(typeof addProductQuantityCount.value)
    if (/^\d+$/.test(addProductQuantityCount.value) === true) {
      const addProductQuantityCountInt: number = +addProductQuantityCount.value
      const productQuantityInt: number = +productQuantity.value
      const sumQuantity = productQuantityInt + addProductQuantityCountInt
      try {
        const res = await productService.updateProductQuantity(id, sumQuantity);
        console.log(res)
      } catch (e) {
        console.log(e);
      } addProductQuantityCount.value = "";
      addProductQuantityStatus.value = false;
      productQuantity.value = 0;
      await getProducts();
    } else {
      addProductQuantityCount.value = "";
      return;
    }
  }

  return {
    products,
    getProducts,
    dialog,
    editedProduct,
    saveProduct,
    editProduct,
    deleteProduct,
    category,
    getProductsByCategory,
    page,
    take,
    keyword,
    lastPage,
    productGetID,
    gotProduct,
    getProductDetail,
    productId,
    count,
    increaseCount,
    decreaseCount,
    addProductQuantity,
    addProductQuantityStatus,
    addProductQuantityCount,
    productQuantity,
    // product
  };
});
