import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type Delivery from '@/types/Delivery'
import deliveryService from '../services/delivery'
export const useDeliveryStore = defineStore('Delivery', () => {
  const delivery = ref<Delivery[]>([])
  const deliveryById = ref<Delivery | undefined>(undefined)
  const dialog = ref(false)
  const deliveryId = ref<number | null>(null)
  const Status = ref<number | null>(null)
  //เก็บ โชว์ แก้ไข ข้อมูลในเดลิเวอร์ลี่
  const editeddelivery = ref<Delivery | undefined>(undefined)

  async function getDeliverys() {
    try {
      const res = await deliveryService.getDeliverys()
      delivery.value = res.data

      console.log('delivery', delivery.value)
    } catch (e) {
      console.log(e)
    }
  }
  async function getDeliveryById() {
    try {
      if (deliveryId.value !== null) {
        const res = await deliveryService.getDeliveryById(deliveryId.value)
        deliveryById.value = res.data
        console.log('deliveryId', deliveryById.value)
        editeddelivery.value = deliveryById.value
      }
    } catch (e) {
      console.log(e)
    }
  }

  //สร้างฟังก์ชั่นมาเก็บข้อมูลในเดลิเวอร์รี่นะจ้ะนายจ๋า
  async function updateDelivery(delivery: Delivery) {
    editeddelivery.value = delivery
    dialog.value = true
    console.log('edited', editeddelivery.value)
  }
  function setDeliveryStatus(status: number) {
    Status.value = status
  }
  async function updateDeliveryBackend(updatedelivery: Delivery) {
    if (editeddelivery.value !== undefined) {
      console.log('Before update:', editeddelivery.value)
      if (Status.value !== null) {
        editeddelivery.value.deliverystatusId = Status.value
        console.log('After update:', editeddelivery.value)
        try {
          await deliveryService.updateDeliverys(editeddelivery?.value.id!, updatedelivery)
          console.log('Booking updated successfully')
        } catch (error) {
          console.error('Error updating booking:', error)
        }
      } else {
        console.error('Status.value is undefined')
      }
    } else {
      console.error('editeddelivery.value is undefined')
    }
  }



  return {
    getDeliverys,
    delivery,
    dialog,
    getDeliveryById,
    deliveryId,
    deliveryById,
    editeddelivery,
    updateDelivery,
    setDeliveryStatus,
    Status,
    updateDeliveryBackend
  }
})
