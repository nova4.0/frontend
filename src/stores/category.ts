import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Category from "@/types/Category";
import categoryService from "@/services/category";
// import { useLoadingStore } from "./loading";
// import { useMessageStore } from "./message";
import Swal from "sweetalert2";
export const useCategoryStore = defineStore("Category", () => {
  // const loadingStore = useLoadingStore();
  // const messageStore = useMessageStore();
  const dialog = ref(false);
  const category = ref<Category[]>([]);
  const editedCategory = ref<Category>({ name: "" });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedCategory.value = { name: "" };
    }
  });
  async function getCategory() {
    // loadingStore.isLoading = true;
    try {
      const res = await categoryService.getCategory();
      category.value = res.data;
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถดึงข้อมูล Category ได้");
    }
    // loadingStore.isLoading = false;
  }

  async function saveCategory() {
    // loadingStore.isLoading = true;
    try {
      if (editedCategory.value.id) {
        const res = await categoryService.updateCategory(
          editedCategory.value.id,
          editedCategory.value
        );
      } else {
        const res = await categoryService.saveCategory(editedCategory.value);
      }

      dialog.value = false;
      await getCategory();
    } catch (e) {
      // messageStore.showError("ไม่สามารถบันทึก Category ได้");
      console.log(e);
    }
    // loadingStore.isLoading = false;
  }

  async function deleteCategory(id: number) {
    const confirmed = await Swal.fire({
      title: "Are you sure?",
      text: "You will not be able to recover this category!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    });

    if (confirmed.isConfirmed) {
      // loadingStore.isLoading = true;
      try {
        const res = await categoryService.deleteCategory(id);
        await getCategory();
        // messageStore.showConfirm("Category deleted successfully");
      } catch (e) {
        console.log(e);
        // messageStore.showError("Failed to delete category");
      }
      // loadingStore.isLoading = false;
    }
  }
  function editCategory(category: Category) {
    editedCategory.value = JSON.parse(JSON.stringify(category));
    dialog.value = true;
  }
  return {
    category,
    getCategory,
    dialog,
    editedCategory,
    saveCategory,
    editCategory,
    deleteCategory,
  };
});
