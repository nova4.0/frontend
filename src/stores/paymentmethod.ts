import { defineStore } from "pinia";
import PaymentmethodService from "../services/paymentmethod";
import type Paymentmethods from "@/types/Paymentmethod";
import { computed, ref, watch } from "vue";


export const usePaymentmethodStore = defineStore("Paymentmethod", () => {
  const Paymentmethods = ref<Paymentmethods[]>([]);
  const selectedPaymentId = ref<null | number>(null);
  const dialog = ref(false);


  const payment = computed(() => {
    const selectedPayment = Paymentmethods.value.find(item => item.id === selectedPaymentId.value);
    return selectedPayment ? selectedPayment.id : 0;
});

  async function getPaymentmethod() {
    try {
      const res = await PaymentmethodService.getPaymentmethod();
      Paymentmethods.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }


  return {
    getPaymentmethod,
    Paymentmethods,
    selectedPaymentId,
    payment,
    dialog
  };
});
