import { defineStore } from 'pinia'
import type Order from '@/types/Order'
import orderService from '../services/order'
import deliveryService from '../services/delivery'
import { ref, computed } from 'vue'
import { useProductStore } from './product'
import { useDeliverymethodStore } from './deliverymethod'
import { usePaymentmethodStore } from './paymentmethod'
import type OrderItem from '@/types/OrderItem'
import order from '../services/order'
import { useCartStore } from './cart'
import type Cart from '@/types/Cart'
import type Paymentmethod from '@/types/Paymentmethod'
import type Deliverymethod from '@/types/Deliverymethod'
import { useDeliveryStore } from './delivery'
import type Delivery from '@/types/Delivery'
const productStore = useProductStore()
const cartStore = useCartStore()
const deliveryStore = useDeliverymethodStore()
const paymentStore = usePaymentmethodStore()
const delivery2Store = useDeliveryStore()

export const useOrderStore = defineStore('Order', () => {
  const creditcardnumber = ref('')
  const expirationdate = ref('')
  const name = ref('')
  const CVVnumber = ref('')
  const address = ref('')
  const uploadImage = ref(false)
  const Orders = ref<Order[]>([])
  const OrderItem = ref<OrderItem[]>([])
  const itemId = ref(0)
  const dialog = ref(false)
  // const editedOrder = ref<Order>({
  //     discount: 0,
  //     subtotal: 0,
  //     totalPrice: 0,
  //     address: "",
  //     slipImage:"no_img_avaliable.jpg",
  //     items: [],
  //     cart: {} as Cart,
  //     paymentMethod: {} as Paymentmethod,
  //     deliveryMethod: {} as Deliverymethod,
  // });
  const editedOrder = ref<Order & { files: File[] }>({
    discount: 0,
    subtotal: 0,
    totalPrice: 0,
    address: '',
    slipImage: '',
    files: [],
    items: [],
    cartId: 0,
    paymentMethodId: 0,
    deliveryMethodId: 0,
    paymentMethod: {} as Paymentmethod,
    deliveryMethod: {} as Deliverymethod
  })
  const editedDelivery = ref<Delivery>({
    orderId: 1,
    userId: 1,
    deliverystatusId: 1,
  })

  function setDeliveryMethod(deliveryMethod: Deliverymethod) {
    //เซ็ตไอดีรูปแบบจัดส่ง
    editedOrder.value.deliveryMethod = deliveryMethod
    console.log(editedOrder.value.deliveryMethod)
    //เซ็ตยอดรวมดึงจากคาร์ทมา
    editedOrder.value.subtotal = cartStore.sumPrice
    console.log('subtotal', editedOrder.value.subtotal)
    //หาไอดีรูปแบบจัดส่งเพื่อดึงราคามาจากไอดี
    // const selectedDelivery = deliveryStore.deliverymethods.find(item => item.id === editedOrder.value.deliveryMethod);
    const selectedDelivery = deliveryStore.deliverymethods.find(
      (item) => item.id === editedOrder.value.deliveryMethod?.id
    )
    //พอดีดึงมาแล้วก็เรียกราคามาบวกกับราคารวมแล้วก็ลบส่วนลดเป็นยอดรวมสุทธิ
    if (selectedDelivery) {
      editedOrder.value.totalPrice =
        cartStore.sumPrice + selectedDelivery.price - editedOrder.value.discount
      console.log('totalPrice', editedOrder.value.totalPrice)
    } else {
      console.log('ไม่พบวิธีการจัดส่งที่ตรงกับ ID')
    }
    return Promise.resolve()
  }

  //เซฟที่อยู่
  async function saveAddress(addressData: string) {
    editedOrder.value.address = addressData
    console.log(editedOrder.value.address)
    return Promise.resolve()
  }
  //usepromo ให้ลด
  async function setDiscount(discount: number) {
    editedOrder.value.discount = discount
    console.log(editedOrder.value.discount)
    editedOrder.value.totalPrice =
      cartStore.sumPrice - editedOrder.value.discount + deliveryStore.deliveryPrice
  }

  async function saveOrder(
    cartId: number,
    addresss: string,
    paymentMethodId: number,
    deliveryMethodId: number,
    discount: number,
    slipImage: string
  ) {
    cartId = 1
    addresss = editedOrder.value.address
    paymentMethodId = paymentStore.selectedPaymentId ?? 0
    deliveryMethodId = deliveryStore.selectedDeliveryId ?? 0
    discount = editedOrder.value.discount
    // slipImage = editedOrder.value.files[0].name;
    if (editedOrder.value.files.length > 0) {
      slipImage = editedOrder.value.files[0].name
    } else {
      slipImage = 'no_img_avaliable.jpg'
    }

    try {
      const res = await orderService.saveOrder(
        cartId,
        addresss,
        paymentMethodId,
        deliveryMethodId,
        discount,
        slipImage
      )
      console.log('ที่อยู่จัดส่ง :', editedOrder.value.address)
      console.log('paymentId :', paymentStore.selectedPaymentId)
      console.log('deliveryId :', deliveryStore.selectedDeliveryId)
      console.log('files :', editedOrder.value.files)
      console.log('ชื่อ slipImage :', slipImage)
      cartStore.clearCartItem()
      deliveryStore.selectedDeliveryId = null
      editedOrder.value.address = ''
      if (paymentMethodId == 1) {
        creditcardnumber.value = ''
        expirationdate.value = ''
        name.value = ''
        CVVnumber.value = ''
        address.value = ''
      } else if (paymentMethodId == 2) {
        editedOrder.value.files = []
        uploadImage.value = false
      }
      paymentStore.selectedPaymentId = null
    } catch (e) {
      console.log(e)
    }
  }

  function setImage() {
    const fileName = 'no_img_available.jpg' // Assuming this is your file name
    const file = new File([], fileName) // Creating a File object with an empty array as content
    editedOrder.value.files = [file]
    console.log('editedOrder.value.files', editedOrder.value.files)
  }

  async function saveOrder1() {
    editedOrder.value.cartId = 1
    editedOrder.value.totalPrice =
      cartStore.sumPrice - editedOrder.value.discount + deliveryStore.deliveryPrice
    editedOrder.value.paymentMethodId =
      paymentStore.selectedPaymentId !== null ? paymentStore.selectedPaymentId : 0
    editedOrder.value.deliveryMethodId =
      deliveryStore.selectedDeliveryId !== null ? deliveryStore.selectedDeliveryId : 0
    console.log('ค่าจัดส่ง', deliveryStore.deliveryPrice)
    console.log('ยอดรวม', cartStore.sumPrice)
    console.log('ส่วนลด', editedOrder.value.discount)
    console.log('ยอดรวมสุทธิ', editedOrder.value.totalPrice)
    console.log('รูปแบบการจัดส่ง', editedOrder.value.deliveryMethodId)
    console.log('วิธีการชำระเงิน', editedOrder.value.paymentMethodId)

    try {
      const res = await orderService.saveOrder1(editedOrder.value)
      console.log('editedOrder.value', editedOrder.value)
      console.log('res', res)

      editedDelivery.value.orderId = res.data.id

      const res2 = await deliveryService.saveDelivery(editedDelivery.value)
      console.log('saveDelivery', res2)
      console.log('saveDelivery', deliveryService.saveDelivery)

      cartStore.clearCartItem()
      deliveryStore.selectedDeliveryId = null
      editedOrder.value.address = ''
      if (paymentStore.selectedPaymentId == 1) {
        creditcardnumber.value = ''
        expirationdate.value = ''
        name.value = ''
        CVVnumber.value = ''
        address.value = ''
      } else if (paymentStore.selectedPaymentId == 2) {
        editedOrder.value.files = []
        uploadImage.value = false
      }
      paymentStore.selectedPaymentId = null
    } catch (e) {
      console.log(e)
    }
  }

  const sumPrice = computed(() => {
    let sum = 0
    for (let i = 0; i < OrderItem.value.length; i++) {
      sum = sum + OrderItem.value[i].price * OrderItem.value[i].quantity
    }
    return sum
  })

  // async function getOrderById() {
  //     // loadingStore.isLoading = true;
  //     try {
  //         const res = await orderService.getOrderById(94);
  //         Orders.value = res.data;
  //         console.log("Orders",Orders);
  //     } catch (e) {
  //         console.log(e);
  //     }
  // }

  async function getOrderById() {
    try {
      const res = await orderService.getOrderById(94)
      Orders.value = res.data
      console.log('Orders', Orders)
    } catch (e) {
      console.log(e)
    }
  }

  return {
    Orders,
    OrderItem,
    itemId,
    sumPrice,
    dialog,
    editedOrder,
    saveAddress,
    setDeliveryMethod,
    saveOrder,
    creditcardnumber,
    expirationdate,
    name,
    CVVnumber,
    address,
    uploadImage,
    setDiscount,
    saveOrder1,
    setImage,
    getOrderById
  }
})
