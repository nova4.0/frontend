import { ref, watch } from "vue";
import { defineStore } from "pinia";

import type Promotion from "@/types/Promotion";
import promotion from "../services/promotion";
import PromotionService from "../services/promotion";

// import { useLoadingStore } from "./loading";
// import { useMessageStore } from "./message";

export const usePromotionStore = defineStore("Promotion", () => {
  // const loadingStore = useLoadingStore();
  // const messageStore = useMessageStore();
  const dialog = ref(false);
  const usePro = ref(false);
  const promotions = ref<Promotion[]>([]);
  const editedPromotion = ref<Promotion>({
    name: "",
    description: "",
    discount: 0,
    startDate: new Date(),
    endDate: new Date(),

  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedPromotion.value = {
        name: "",
        description: "",
        discount: 0,
        startDate: new Date(),
        endDate: new Date(),

      };
    }
  });
  async function getPromotion() {
    // loadingStore.isLoading = true;
    try {
      const res = await PromotionService.getPromotions();
      promotions.value = res.data;
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถดึงข้อมูล Promotion ได้");
    }
    // loadingStore.isLoading = false;
  }

  async function savePromotion() {
    // loadingStore.isLoading = true;
    try {
      if (editedPromotion.value.id) {
        const res = await PromotionService.updatePromotion(
          editedPromotion.value.id,
          editedPromotion.value
        );
      } else {
        const res = await PromotionService.savePromotion(editedPromotion.value);
      }

      dialog.value = false;
      await getPromotion();
    } catch (e) {
      // messageStore.showError("ไม่สามารถบันทึก Product ได้");
      console.log(e);
    }
    // loadingStore.isLoading = false;
  }

  async function deletePromotion(id: number) {
    // loadingStore.isLoading = true;
    try {
      const res = await PromotionService.deletePromotion(id);
      await getPromotion();
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถลบ Promotion ได้");
    }
    window.location.reload();
    // loadingStore.isLoading = false;
  }

  function editPromotion(promotion: Promotion) {
    editedPromotion.value = JSON.parse(JSON.stringify(promotion));
    dialog.value = true;
  }
  return {
    promotions,
    getPromotion,
    dialog,
    editedPromotion,
    savePromotion,
    editPromotion,
    deletePromotion,
    usePro
  };
});
