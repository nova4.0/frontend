import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Supplier from "@/types/Supplier";
import supplierService from "@/services/supplier";
// import { useLoadingStore } from "./loading";
// import { useMessageStore } from "./message";
import Swal from "sweetalert2";
export const useSupplierStore = defineStore("Supplier", () => {
  // const loadingStore = useLoadingStore();
  // const messageStore = useMessageStore();
  const dialog = ref(false);
  const supplier = ref<Supplier[]>([]);
  const gotSupplier = ref<Supplier>({
    name: "",
  });
  const supplierId = ref(0);
  function getSupplierbyId(supplier: Supplier) {
    gotSupplier.value = JSON.parse(JSON.stringify(supplier));
    if (supplier.id !== undefined) {
      supplierId.value = supplier.id;
    }
    console.log(supplier);
  }
  function getSupplierName(id:number){
    for(let i = 0;i<supplier.value.length;i++){
      if(id-1==i){
      gotSupplier.value.name = supplier.value[i].name;
    }
    }
  }
  const editedSupplier = ref<Supplier>({ name: "" });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedSupplier.value = { name: "" };
    }
  });
  async function getSupplier() {
    // loadingStore.isLoading = true;
    try {
      const res = await supplierService.getSupplier();
      supplier.value = res.data;
    } catch (e) {
      console.log(e);
      // messageStore.showError("ไม่สามารถดึงข้อมูล Supplier ได้");
    }
    // loadingStore.isLoading = false;
  }

  return {
    supplier,
    getSupplier,
    dialog,
    editedSupplier,
    getSupplierbyId,
    gotSupplier,
    getSupplierName
  };
});
