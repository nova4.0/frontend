import { defineStore } from "pinia";
import DeliverymethodService from "../services/deliverymethod";
import type Deliverymethods from "@/types/Deliverymethod";
import { computed, ref } from "vue";


export const useDeliverymethodStore = defineStore("Deliverymethod", () => {
  const deliverymethods = ref<Deliverymethods[]>([]);
  const selectedDeliveryId = ref<null | number>(null);

  const deliveryPrice = computed(() => {
    const selectedDelivery = deliverymethods.value.find(item => item.id === selectedDeliveryId.value);
    return selectedDelivery ? selectedDelivery.price : 0;
  });

  async function getDeliverymethod() {
    try {
      const res = await DeliverymethodService.getDeliverymethod();
      deliverymethods.value = res.data;
      console.log(deliverymethods);
    } catch (e) {
      console.log(e);
    }
  }


  return {
    getDeliverymethod,
    deliverymethods,
    deliveryPrice,
    selectedDeliveryId
  };
});
