import { defineStore } from 'pinia'
import { ref } from 'vue'
import type Deliverystatus from '@/types/Deliverystatus'
import deliverystatusService from '@/services/deliverystatus'

export const useDeliverystatusStore = defineStore('Deliverystatus', () => {
  const deliverystatus = ref<Deliverystatus[]>([])
  const selecteddeliverystatusId = ref<null | number>(null)

  async function getdeliverystatus() {
    try {
      const res = await deliverystatusService.getDeliverystatus()
      deliverystatus.value = res.data
    } catch (e) {
      console.log(e)
    }
  }

  return {
    getdeliverystatus,
    deliverystatus,
    selecteddeliverystatusId
  }
})
