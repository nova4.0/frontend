import { defineStore } from "pinia";
import type Cart from "@/types/Cart";
import cartService from "../services/cart";
import { ref, computed } from "vue";
import { useProductStore } from "./product";
import type CartItem from "@/types/CartItem";
const productStore = useProductStore();


export const useCartStore = defineStore("Cart", () => {
    const carts = ref<Cart[]>([]);
    const cartItem = ref<CartItem[]>([]);
    const itemId = ref(0)
    // const cartItemCount = ref(cartItem.value.length);
    const cartItemCount = computed(() => {
        return cartItem.value.length;
    });

    async function getCartItem() {
        const cartId = 1
        try {
            const res = await cartService.getCartItem(cartId);
            cartItem.value = res.data;
            console.log(cartItem, "cartStore.cartItem");
        } catch (e) {
            console.log(e);
        }
    }

    async function updateCartItemQuantity(quantity: number, itemId: number) {
        console.log(itemId)
        console.log(quantity)
        try {
            const res = await cartService.updateCartItemQuantity(quantity, itemId);
            console.log(res)
            
        } catch (e) {
            console.log(e);
        }
    }

    async function saveProductToCart() {
        const cartId = 1
        const productId = productStore.productId
        const quantity = productStore.count
        getCartItem()
        try {
            for (let i = 0; i < cartItem.value.length; i++) {
                if (cartItem.value[i].product.id === productId) {
                    cartItem.value[i].quantity = cartItem.value[i].quantity + productStore.count;
                    updateCartItemQuantity(cartItem.value[i].quantity, cartItem.value[i].id!);
                    productStore.count = 1;
                    getCartItem()
                    return;
                }
            }
            const res = await cartService.saveProductToCart(cartId, productId, quantity);
            console.log(res)
            getCartItem()
        } catch (e) {
            console.log(e);
        }
        productStore.count = 1
    }

    async function deleteCartItem(itemId: number) {
        console.log(itemId);
        try {
            const res = await cartService.deleteCartItem(itemId);
            await getCartItem();

        } catch (e) {
            console.log(e);
        }
        // window.location.reload();
    }
    async function clearCartItem() {
        try {
            for (let i = 0; i < cartItem.value.length; i++) {
                deleteCartItem(cartItem.value[i].id!)
            }
        } catch (e) {
            console.log(e);
        }
        getCartItem();
    }

    const sumPrice = computed(() => {
        let sum = 0;
        for (let i = 0; i < cartItem.value.length; i++) {
            sum = sum + (cartItem.value[i].product.price * cartItem.value[i].quantity);
        }
        return sum;
    });
    

    return {
        getCartItem,
        deleteCartItem,
        saveProductToCart,
        carts,
        cartItem,
        itemId,
        updateCartItemQuantity,
        sumPrice,
        cartItemCount,
        clearCartItem
    };
});
