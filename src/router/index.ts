import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",

      components: {
        default: () => import("../views/HomeView.vue"),
      }
    },

    // {
    //   path: '/login',
    //   name: 'login',

    //   components: {
    //     default: () => import('../views/LoginDialog.vue')
    //   },

    //   meta: {
    //     login: true
    //   }
    // },
    {
      path: "/management",
      name: "management",

      components: {
        default: () => import("../views/management/ManagementView.vue"),
      }
    },
    {
      path: "/promotion",
      name: "promotion",

      components: {
        default: () => import("../views/management/promotion/PromotionView.vue"),
      }
    },
    {
      path: "/stockproduct",
      name: "stockproduct",

      components: {
        default: () => import("../views/management/stock/stockProductView.vue"),
      }
    },
    {
      path: "/report",
      name: "report",

      components: {
        default: () => import("../views/management/report/ReportView.vue"),
      }
    }
    ,
    {
      path: "/manubuyproductsuplier",
      name: "manubuyproductsuplier",

      components: {
        default: () => import("../views/management/supplier/MenuBuyProductSupplierView.vue"),
      }
    },
    {
      path: "/receiptsupplier",
      name: "receiptsupplier",

      components: {
        default: () => import("../views/management/supplier/ReceiptSupplier.vue"),
      }
    },
    {
      path: "/buyproductsupplier",
      name: "buyproductsupplier",

      components: {
        default: () => import("../views/management/supplier/BuyProductSupplierView.vue"),
      }
    },
    {
      path: "/delivery",
      name: "delivery",

      components: {
        default: () => import("../views/management/delivery/DeliveryView.vue"),
      }
    },
    {
      path: "/productclaim",
      name: "productclaim",

      components: {
        default: () => import("../views/management/claim/ProductClaimView.vue"),
      }
    },
    {
      path: "/manageaccount",
      name: "manageaccount",

      components: {
        default: () => import("../views/management/manageacc/ManageAccountView.vue"),
      }
    },
    {
      path: "/adjuststock",
      name: "adjuststock",

      components: {
        default: () => import("../views/management/stock/AdjustStockView.vue"),
      }
    },
    {
      path: "/productsaledetail",
      name: "productsaledetail",

      components: {
        default: () => import("../views/management/sales/ProductSaleDetailView.vue"),
      }
    },
    {
      path: "/cartview",
      name: "cartview",

      components: {
        default: () => import("../views/management/sales/cartView.vue"),
      }
    },
    {
      path: "/summaryproductsale",
      name: "summaryproductsale",

      components: {
        default: () => import("../views/management/sales/SummaryProductSaleView.vue"),
      }
    },
    {
      path: "/qrpayment",
      name: "qrpayment",

      components: {
        default: () => import("../views/management/sales/QrPaymentView.vue"),
      }
    },
    {
      path: "/qrpayment",
      name: "qrpayment",

      components: {
        default: () => import("../views/management/sales/QrPaymentView.vue"),
      }
    },
    {
      path: "/paymentsuccess",
      name: "paymentsuccess",

      components: {
        default: () => import("../views/management/sales/PaymentSuccessView.vue"),
      }
    }
    ,
    {
      path: "/creditpayment",
      name: "creditpayment",

      components: {
        default: () => import("../views/management/sales/CreditPaymentView.vue"),
      }
    },
    {
      path: "/confirmcreditpayment",
      name: "confirmcreditpayment",

      components: {
        default: () => import("../views/management/sales/ConfirmCreditPaymentView.vue"),
      }
    },
  ]
})

export default router
