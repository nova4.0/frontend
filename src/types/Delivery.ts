import type Order from "./Order";
import type User from "./User";
import type Deliverymethod from "./Deliverymethod";
import type Paymentmethod from "./Paymentmethod";
import type Deliverystatus from "./Deliverystatus";
export default interface Delivery {
    id?: number;
    orderId: number;
    userId: number;
    deliverystatusId: number;
    order?: Order;
    user?: User;
    paymentmethoder?: Paymentmethod; // ตรวจสอบให้แน่ใจว่าชื่อเป็น 'paymentmethoder' ถูกต้องและไม่ซ้ำในที่อื่น
    deliverymethod?: Deliverymethod; // ตรวจสอบให้แน่ใจว่าชื่อเป็น 'deliverymethod' ถูกต้องและไม่ซ้ำในที่อื่น
    deliverystatus?: Deliverystatus[];
    
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
}
