import type Cart from "./Cart";
import type Deliverymethod from "./Deliverymethod";
import type OrderItem from "./OrderItem";
import type Paymentmethod from "./Paymentmethod";

export default interface Order {
  id?: number;
  discount: number;
  subtotal: number;
  totalPrice: number;
  address: string;
  slipImage: string;
  items: OrderItem[];
  // cart: Cart;
  // paymentMethod: Paymentmethod;
  // deliveryMethod: Deliverymethod;
  cartId: number;
  paymentMethodId: number;
  deliveryMethodId: number;
  deliveryMethod: Deliverymethod;
  paymentMethod: Paymentmethod;

  createdDate?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
