export default interface Product {
  id?: number;
  name: string;
  description: string;
  price: number;
  minquantity: number;
  qtybefore: number;
  image: string;
  categoryId: number;

  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
