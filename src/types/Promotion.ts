export default interface Promotion {
    id?: number;
    name: string;
    description: string;
    discount: number;
    startDate: Date;
    endDate: Date;
    
  
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
  }
  