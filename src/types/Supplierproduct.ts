export default interface Supplierproduct {
  id?: number;
  name: string;
  description: string;
  price: number;
  minquantity: number;
  qtybefore: number;
  image: string;
  supplierId: number;

  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}