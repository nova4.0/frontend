import type Delivery from "./Delivery";
export default interface Deliverystatus {
  id?: number;
  name : string;
  delivery: Delivery;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
