export default interface Stockproduct {
  id?: number;
  qtyafter: number;
  productId: number;

  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
