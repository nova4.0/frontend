export default interface Customer {
    id?: number;
    name: String;
    telNumber: String;

    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
  }
  