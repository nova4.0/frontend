import type Customer from "./Customer";
import type CartItem from "./CartItem";
export default interface Cart {
  id?: number;
  customer: Customer;
  items?: CartItem[];

  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
