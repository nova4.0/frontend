import type Cart from "./Cart";
import type Product from "./Product";
export default interface CartItem {
    id?: number;
    product: Product;
    quantity: number;
    cart?: Cart;

    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
  }
  