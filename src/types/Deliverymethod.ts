export default interface Deliverymethod {
    id?: number;
    name: String;
    price: number;
    description: string;
}
