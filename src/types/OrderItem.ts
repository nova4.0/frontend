import type Order from "./Order";
import type Product from "./Product";

export default interface OrderItem {
  id?: number;
  product: Product;
  quantity: number;
  price: number;
  order: Order;


  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
